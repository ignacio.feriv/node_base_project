const express = require('express');

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
const getUsers = (req,res) => {
  const users = [
    {
      id: 1,
      name: 'Juan Carlos',
    },
    {
      id: 2,
      name: 'Juan José',
    }
  ];

  res.json(users);
}

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
const createUser = (req,res) => {
  const user = req.body;
  user.id = 38743;

  const result = {
    message: 'User created',
    user
  }

  res.status(201).json(result);
}

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
const updateUser = (req,res) => {
  const { id } = req.params;
  const user = req.body;

  user.id = id;

  const result = {
    message: 'User updated',
    user
  }

  res.status(201).json(result);
}

/**
 * 
 * @param {express.Request} req 
 * @param {express.Response} res 
 */
const deleteUser = (req,res) => {
  const { id } = req.params;
  const result = {
    message: `User with id ${ id } has been deleted.`,
    user
  }

  res.status(201).json(result);
}

module.exports = {
  getUsers,
  createUser,
  updateUser,
  deleteUser
}